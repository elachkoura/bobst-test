import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { MapperService } from '../mapper/mapper.service';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(
    private networkService: NetworkService,
    private mappersService: MapperService
  ) { }

  public getPeoplesList(page?) {
    return this.networkService
      .get(page)
      .then(data => {
        const Data = this.mappersService._mapJsonToData(data);
        return Promise.resolve(Data);
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }

  public getOnePeople(id) {
    return this.networkService
      .getOne(id)
      .then(data => {
        const Data = this.mappersService._mapJsonToPeople(data);
        return Promise.resolve(Data);
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }
}
