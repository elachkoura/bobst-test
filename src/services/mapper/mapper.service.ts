import { Injectable } from '@angular/core';
import { People } from '../../models/People';
import * as moment from 'moment';
import { Data } from 'src/models/data';
@Injectable({
  providedIn: 'root'
})
export class MapperService {
  constructor() {}

  // People Mapper
  public _mapJsonToPeoples(arrayOfPeople): Array<People> {
    return arrayOfPeople.map(data => this._mapJsonToPeople(data));
  }

  public _mapJsonToPeople(peopleJson): People {
    const people = new People();

    people.name = peopleJson['name'];
    people.height = peopleJson['height'];
    people.mass = peopleJson['mass'];
    people.hair_color = peopleJson['hair_color'];
    people.gravatar_id = peopleJson['gravatar_id'];
    people.skin_color = peopleJson['skin_color'];
    people.eye_color = peopleJson['eye_color'];
    people.birth_year = peopleJson['birth_year'];
    people.following_url = peopleJson['following_url'];
    people.homeworld = peopleJson['homeworld'];
    people.gender = peopleJson['gender'];
    people.received_events_url = peopleJson['received_events_url'];
    people.films = peopleJson['films'];
    people.species = peopleJson['species'];
    people.vehicles = peopleJson['vehicles'];
    people.starships = peopleJson['starships'];
    people.created = peopleJson['created'];
    people.edited = peopleJson['edited'];
    people.url = peopleJson['url'];

    const arr = people.url.split('/');
    people.url = people.url.split('/')[arr.length - 2];
    return people;
  }

  // Data Mapper
  public _mapJsonToData(dataJson): Data {
    const data = new Data();

    data.count = dataJson['count'];
    data.next = dataJson['next'];
    data.previous = dataJson['previous'];
    data.previous = dataJson['previous'];
    data.results = dataJson['results'] ? this._mapJsonToPeoples(dataJson['results']) : [];

    return data;
  }
}
