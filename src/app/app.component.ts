import { Component, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bobst-test';
  langArray: Array<any> = environment.supportedLangs;
  storedLang = environment.defaultLanguage;

  constructor(
    private translateService: TranslateService,
    private renderer: Renderer2
  ) {
    if (localStorage.getItem('language')) {
      this.storedLang = localStorage.getItem('language');
    }

    let currentLang: string = null;
    this.langArray.map(item => {
      if (item.lang === this.storedLang) {
        item.isChecked = true;
        currentLang = item.lang;
      } else {
        item.isChecked = false;
      }
    });

    if (currentLang) {
      // setting selected lang
      this.translateService.setDefaultLang(currentLang);
      this.translateService.use(currentLang);
      localStorage.setItem('language', currentLang);
    }

    this.renderer.setAttribute(document.querySelector('html'), 'lang', currentLang);
  }
}
