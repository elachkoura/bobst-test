export const environment = {
  production: true,
  base: 'https://swapi.dev/api/people/',
  defaultLanguage: 'en',
  supportedLangs: [
    {
      lang: 'en',
      langTitle: 'English',
      isChecked: false
    },
    {
      lang: 'fr',
      langTitle: 'Français',
      isChecked: false
    }
  ]
};
