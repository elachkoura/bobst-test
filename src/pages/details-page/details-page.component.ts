import { Component, OnInit } from '@angular/core';
import { People } from '../../models/People';
import { ActivatedRoute, Router } from '@angular/router';
import { PeopleService } from 'src/services/people/people.service';
import { Data } from 'src/models/data';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {
  public currentPeople: People = null;
  public genericError: string = null;
  public peopleId = '';

  constructor(
    private peopleService: PeopleService,
    private activatedRoute: ActivatedRoute
  ) {
    this.peopleId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getCurrentPeople(this.peopleId);
  }

  /**
   * Get current people
   */
  public getCurrentPeople(peopleId: string) {
    this.peopleService
      .getOnePeople(peopleId)
      .then(people => {
        console.log(people);
        this.currentPeople = people;
      }).catch(error => {
        this.genericError = error;
        // Handle error gently
      });
  }
}
