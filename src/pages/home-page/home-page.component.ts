import { Component, OnInit } from '@angular/core';
import { People } from '../../models/People';
import { PeopleService } from 'src/services/people/people.service';
import { faArrowLeft, faArrowRight, faMars, faVenus, faVenusMars } from '@fortawesome/free-solid-svg-icons';
import { Data } from 'src/models/data';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public dataResult: Data = null;
  public peoplesList: Array<People> = [];
  public genericError: string = null;
  public nextPage = null;
  public prevPage = null;
  storedLang = localStorage.getItem('language');
  langArray: Array<any> = environment.supportedLangs;

  faMars = faMars;
  faVenus = faVenus;
  faVenusMars = faVenusMars;
  faArrowLeft = faArrowLeft;
  faArrowRight = faArrowRight;

  constructor(
    private peopleService: PeopleService,
    private translateService: TranslateService
  ) {
    this.getPeoplesList();
  }

  ngOnInit() {

  }

  /**
   * Select language
   */
  public selectLang(langItem) {
    this.langArray.map(item => {
      if (item.lang === langItem.lang) {
        item.isChecked = true;
        this.storedLang = item.lang;
      } else {
        item.isChecked = false;
      }
    });

    if (this.storedLang) {
      // setting selected lang
      this.translateService.setDefaultLang(this.storedLang);
      this.translateService.use(this.storedLang);
      localStorage.setItem('language', this.storedLang);
      window.location.reload();
    }
  }

  /**
   * Get lang array
   */
  public getLangArray(): Array<any> {
    return this.langArray;
  }

  /**
   * Get List of peoples
   */
  public getPeoplesList(page?: string) {
    this.peopleService
      .getPeoplesList(page)
      .then(data => {
        this.dataResult = data;
        this.peoplesList = data.results;

        if (this.dataResult.next) {
          const arr = this.dataResult.next.split('/');
          const nextPageText = this.dataResult.next.split('/')[arr.length - 1];
          this.nextPage = nextPageText.split('=')[1];
        }

        if (this.dataResult.previous) {
          const arrPrev = this.dataResult.previous.split('/');
          const prevPageText = this.dataResult.previous.split('/')[arrPrev.length - 1];
          this.prevPage = prevPageText.split('=')[1];
        }
      }).catch(error => {
        this.genericError = error;
        // Handle error gently
      });
  }

  /**
   * Get gender Icon
   */
  public getGenderIcon(gender): any {
    if (gender === 'male') {
      return faMars;
    } else if (gender === 'female') {
      return faVenus;
    } else {
      return faVenusMars;
    }
  }

  /**
   * Search people
   */
  public searchPeople(event, username) {
    event.preventDefault();
  }
}
