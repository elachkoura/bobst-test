export class People {
  private _name: string = null;
  private _height: string = null;
  private _mass: string = null;
  private _hair_color: string = null;
  private _gravatar_id: string = null;
  private _skin_color: string = null;
  private _eye_color: string = null;
  private _birth_year: string = null;
  private _following_url: string = null;
  private _gender: string = null;
  private _homeworld: string = null;
  private _films: Array<string> = null;
  private _species: Array<string> = [];
  private _vehicles: Array<string> = [];
  private _starships: Array<string> = [];
  private _received_events_url: string = null;
  private _created: string = null;
  private _edited: string = null;
  private _url: string = null;

    /**
     * Getter name
     * @return {string }
     */
	public get name(): string  {
		return this._name;
	}

    /**
     * Getter height
     * @return {string }
     */
	public get height(): string  {
		return this._height;
	}

    /**
     * Getter mass
     * @return {string }
     */
	public get mass(): string  {
		return this._mass;
	}

    /**
     * Getter hair_color
     * @return {string }
     */
	public get hair_color(): string  {
		return this._hair_color;
	}

    /**
     * Getter gravatar_id
     * @return {string }
     */
	public get gravatar_id(): string  {
		return this._gravatar_id;
	}

    /**
     * Getter skin_color
     * @return {string }
     */
	public get skin_color(): string  {
		return this._skin_color;
	}

    /**
     * Getter eye_color
     * @return {string }
     */
	public get eye_color(): string  {
		return this._eye_color;
	}

    /**
     * Getter birth_year
     * @return {string }
     */
	public get birth_year(): string  {
		return this._birth_year;
	}

    /**
     * Getter following_url
     * @return {string }
     */
	public get following_url(): string  {
		return this._following_url;
	}

    /**
     * Getter gender
     * @return {string }
     */
	public get gender(): string  {
		return this._gender;
	}

    /**
     * Getter homeworld
     * @return {string }
     */
	public get homeworld(): string  {
		return this._homeworld;
	}

    /**
     * Getter films
     * @return {Array<string> }
     */
	public get films(): Array<string>  {
		return this._films;
	}

    /**
     * Getter species
     * @return {Array<string> }
     */
	public get species(): Array<string>  {
		return this._species;
	}

    /**
     * Getter vehicles
     * @return {Array<string> }
     */
	public get vehicles(): Array<string>  {
		return this._vehicles;
	}

    /**
     * Getter starships
     * @return {Array<string> }
     */
	public get starships(): Array<string>  {
		return this._starships;
	}

    /**
     * Getter received_events_url
     * @return {string }
     */
	public get received_events_url(): string  {
		return this._received_events_url;
	}

    /**
     * Getter created
     * @return {string }
     */
	public get created(): string  {
		return this._created;
	}

    /**
     * Getter edited
     * @return {string }
     */
	public get edited(): string  {
		return this._edited;
	}

    /**
     * Getter url
     * @return {string }
     */
	public get url(): string  {
		return this._url;
	}

    /**
     * Setter name
     * @param {string } value
     */
	public set name(value: string ) {
		this._name = value;
	}

    /**
     * Setter height
     * @param {string } value
     */
	public set height(value: string ) {
		this._height = value;
	}

    /**
     * Setter mass
     * @param {string } value
     */
	public set mass(value: string ) {
		this._mass = value;
	}

    /**
     * Setter hair_color
     * @param {string } value
     */
	public set hair_color(value: string ) {
		this._hair_color = value;
	}

    /**
     * Setter gravatar_id
     * @param {string } value
     */
	public set gravatar_id(value: string ) {
		this._gravatar_id = value;
	}

    /**
     * Setter skin_color
     * @param {string } value
     */
	public set skin_color(value: string ) {
		this._skin_color = value;
	}

    /**
     * Setter eye_color
     * @param {string } value
     */
	public set eye_color(value: string ) {
		this._eye_color = value;
	}

    /**
     * Setter birth_year
     * @param {string } value
     */
	public set birth_year(value: string ) {
		this._birth_year = value;
	}

    /**
     * Setter following_url
     * @param {string } value
     */
	public set following_url(value: string ) {
		this._following_url = value;
	}

    /**
     * Setter gender
     * @param {string } value
     */
	public set gender(value: string ) {
		this._gender = value;
	}

    /**
     * Setter homeworld
     * @param {string } value
     */
	public set homeworld(value: string ) {
		this._homeworld = value;
	}

    /**
     * Setter films
     * @param {Array<string> } value
     */
	public set films(value: Array<string> ) {
		this._films = value;
	}

    /**
     * Setter species
     * @param {Array<string> } value
     */
	public set species(value: Array<string> ) {
		this._species = value;
	}

    /**
     * Setter vehicles
     * @param {Array<string> } value
     */
	public set vehicles(value: Array<string> ) {
		this._vehicles = value;
	}

    /**
     * Setter starships
     * @param {Array<string> } value
     */
	public set starships(value: Array<string> ) {
		this._starships = value;
	}

    /**
     * Setter received_events_url
     * @param {string } value
     */
	public set received_events_url(value: string ) {
		this._received_events_url = value;
	}

    /**
     * Setter created
     * @param {string } value
     */
	public set created(value: string ) {
		this._created = value;
	}

    /**
     * Setter edited
     * @param {string } value
     */
	public set edited(value: string ) {
		this._edited = value;
	}

    /**
     * Setter url
     * @param {string } value
     */
	public set url(value: string ) {
		this._url = value;
	}

}
