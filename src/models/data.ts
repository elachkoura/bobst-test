import { People } from './People';

export class Data {
  private _count: number = null;
  private _next: string = null;
  private _previous: string = null;
  private _results: Array<People> = [];

    /**
     * Getter count
     * @return {number }
     */
	public get count(): number  {
		return this._count;
	}

    /**
     * Getter next
     * @return {string }
     */
	public get next(): string  {
		return this._next;
	}

    /**
     * Getter previous
     * @return {string }
     */
	public get previous(): string  {
		return this._previous;
	}

    /**
     * Getter results
     * @return {Array<People> }
     */
	public get results(): Array<People>  {
		return this._results;
	}

    /**
     * Setter count
     * @param {number } value
     */
	public set count(value: number ) {
		this._count = value;
	}

    /**
     * Setter next
     * @param {string } value
     */
	public set next(value: string ) {
		this._next = value;
	}

    /**
     * Setter previous
     * @param {string } value
     */
	public set previous(value: string ) {
		this._previous = value;
	}

    /**
     * Setter results
     * @param {Array<People> } value
     */
	public set results(value: Array<People> ) {
		this._results = value;
	}

}
